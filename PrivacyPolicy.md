# Privacy Policy for TickTock Reminder App

**Effective Date:** December 7, 2023

## 1. Introduction

Thank you for choosing TickTock Reminder! This Privacy Policy outlines how we collect, use, disclose, and safeguard your personal information when you use our mobile application and related services. By installing, accessing, or using the TickTock Reminder app, you agree to the terms outlined in this Privacy Policy.

## 2. Information We Collect

- **Personal Information:**
  We do not collect personal information such as your name, address, or contact details.

- **Usage Information:**
  We collect non-personal information related to the usage of the app, including but not limited to device information, operating system, and app interactions. This data helps us improve the app's performance and user experience.

- **Reminders and Tasks:**
  The app may collect and store the reminders and tasks you create for functionality and synchronization purposes.

## 3. How We Use Your Information

- **App Functionality:**
  We use collected information to provide, maintain, and improve the functionality of the TickTock Reminder app.

- **Analytics:**
  Non-personal information is utilized for analytical purposes to understand user behavior, preferences, and trends. This helps us enhance the app's features and performance.

- **Communication:**
  We may use your contact information (if provided) to respond to inquiries, provide support, and communicate important updates related to the app.

## 4. Data Security

We prioritize the security of your information and employ industry-standard measures to protect against unauthorized access, alteration, disclosure, or destruction.

## 5. Third-Party Services

TickTock Reminder may utilize third-party services for analytics, advertising, or other functionalities. These services may collect information in accordance with their own privacy policies. We encourage you to review the privacy policies of these third-party services.

## 6. Children's Privacy

TickTock Reminder is not directed to individuals under the age of 13. We do not knowingly collect personal information from children. If you believe that a child has provided us with personal information, please contact us, and we will take steps to delete such information.

## 7. Changes to this Privacy Policy

We reserve the right to update or modify this Privacy Policy at any time. We will notify users of any significant changes by posting the updated policy on our website or within the app.

## 8. Contact Us

If you have any questions, concerns, or requests regarding this Privacy Policy, please contact us at helpdevz@gmail.com.

By using TickTock Reminder, you acknowledge that you have read and understood this Privacy Policy and agree to the collection and use of your information as outlined herein.
